import React, { Component  } from 'react';
import './style.css';

export default class LogForm extends Component {
    constructor(props){
        super(props);
        this.state = {
            login: '',
            password: '',
            error: true,
            errorText: ''
        }
        this.onLoginChange = this.onLoginChange.bind(this);
        this.onPasswordChange = this.onPasswordChange.bind(this);
        this.setError = this.setError.bind(this);
        this.onClick = this.onClick.bind(this);
    }
    onLoginChange(ev){
        ev.preventDefault();
        this.setState({ login: ev.target.value })
    }
    onPasswordChange(ev){
        ev.preventDefault();
        this.setState({ password: ev.target.value})
    }
    setError(bool, text){
        this.setState({ error: bool, errorText: text})
    }
    onClick(ev){
        ev.preventDefault();
        const { setError } = this;
        fetch('http://api.pet4u.com.ua/api/v1/account/login/', {
            headers: {
                'Content-type': 'application/json'
            },
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify({email: this.state.login, password: this.state.password})
        })
            .then((res) => {
                if(res.status === 400){
                     setError(true, 'Неправильный пароль.')
                    setTimeout(() =>  setError(false, ''), 3000);
                } else if(res.status === 405){
                     setError(true, 'Права доступа ограничены.')
                    setTimeout(() =>  setError(false, ''), 3000);
                } else {
                    return res.json()
                }
            })
            .then(function(res){
                if(res.token && res.token !== 'undefined') {
                    window.localStorage.setItem('token', res.token);
                    window.location.reload();
                }
            })
            .catch((err) => console.log(err))
    }
    render(){
        return (
            <div className="wrap">
            <div className="form">
                <div className="form-group">
                    <label htmlFor="email">Логин</label>
                    <input type="text" id="email" className="form-control" value={this.state.login} onChange={this.onLoginChange}/>
                </div>
                <div className="form-group">
                    <label>Пароль</label>
                    <input type="password" className="form-control" value={this.state.password} onChange={this.onPasswordChange}/>
                </div>
                {this.state.error ? this.state.errorText : ''}
                <button className="btn btn-primary" onClick={this.onClick}>Войти</button>
            </div>
            </div>
        );
    }
}