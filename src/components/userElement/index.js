import React, {Component} from "react";
import PairItem from './pairItem';
import Calendar from '../Calendar';
import EditProfile from './editProfile';


export default class TableElement extends Component {
    constructor(props) {
        super(props);
        this.state = {
            show: false,
            checked: false,
            edit: null,
            date: '',
            task: '',
            showEdit: false,
            showAddPlanned: false,
            plannedDate: '',
            plannedBreed: '',
            autoBreed: [],
            showAddNewBreed: false,
            breed: '',
            changeComment: false,
            lastCall: ''
        }
        this.onTask = this.onTask.bind(this);
        this.onState = this.onState.bind(this);
        this.onDeleteTask = this.onDeleteTask.bind(this);
        this.onManager = this.onManager.bind(this);
        this.onChecked = this.onChecked.bind(this);
        this.handleFromChange = this.handleFromChange.bind(this);
        this.onClick = this.onClick.bind(this);
        this.onDeleteState = this.onDeleteState.bind(this);
        this.onShowEdit = this.onShowEdit.bind(this);
        this.onDateChange = this.onDateChange.bind(this);
        this.onPlannedBreedChange = this.onPlannedBreedChange.bind(this);
        this.onAuto = this.onAuto.bind(this);
        this.addPlanedPair = this.addPlanedPair.bind(this);
        this.onShowAddBreed = this.onShowAddBreed.bind(this);
        this.onAddBreedChange = this.onAddBreedChange.bind(this);
        this.lastCallChange = this.lastCallChange.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.u !== nextProps.u) {
            return nextProps.u
        }
    }
    lastCallChange(date){
        const value = date.getFullYear() + '-' + (Number(date.getMonth()) + 1) + '-' + date.getDate();
        this.props.onLastCallChange(this.props.u.owner_id, value)
    }
    onAddBreedChange(ev) {
        ev.preventDefault();
        this.setState({breed: ev.target.value})
    }

    onShowBroods(ev) {
        ev.preventDefault()
        this.setState({show: !this.state.show})

    }

    onShowEdit(ev) {
        ev.preventDefault();
        this.setState({showEdit: !this.state.showEdit})
    }

    onTask(ev) {
        ev.preventDefault();
        this.setState({task: ev.target.value})
    }

    handleFromChange(data) {
        this.setState({date: data.getFullYear() + '-' + (Number(data.getMonth()) + 1) + '-' + data.getDate()})
    }

    onShowAddBreed(ev) {
        ev.preventDefault();
        this.setState({showAddNewBreed: !this.state.showAddNewBreed})
    }

    onPlannedBreedChange(ev) {
        let breedsList = [];
        this.props.u.favorite.forEach(b => {
            breedsList.push(b.title)
        });
        this.setState({plannedBreed: ev.target.value});
        breedsList.map(b => {
            if (ev.target.value === '') {
                this.setState({autoBreed: []})
            } else if (b.toUpperCase().includes(ev.target.value.toUpperCase()) && this.state.autoBreed.indexOf(b) === -1) {
                this.setState({autoBreed: [...this.state.autoBreed, b]})
            } else if (!b.toUpperCase().includes(ev.target.value.toUpperCase()) && this.state.autoBreed.indexOf(b) !== -1) {
                let index = this.state.autoBreed.indexOf(b);
                this.setState({autoBreed: [...this.state.autoBreed.slice(0, index), ...this.state.autoBreed.slice(index + 1)]})
            }
        })
    }

    onAuto(ev) {
        this.setState({plannedBreed: ev.target.innerHTML});
    }

    onClick(ev) {
        ev.preventDefault();
        if (this.props.usersId.length > 0) {
            this.props.onAddTask(this.props.usersId, this.state.task, this.state.date);
        } else {
            this.props.onAddTask(this.props.u.owner_id, this.state.task, this.state.date);
        }
        this.setState({date: undefined, task: ''})
    }

    onState(ev) {
        if (this.props.usersId.length > 0) {
            this.props.onChangeState(this.props.usersId, ev.target.value);
        } else {
            this.props.onChangeState(this.props.u.owner_id, ev.target.value);
        }
        ev.target.value = '';
    }

    onDateChange(date) {
        this.setState({plannedDate: date})
        console.log(this.state.plannedBreed, date.getFullYear() + '-' + (Number(date.getMonth()) + 1) + '-' + date.getDate())
    }

    onDeleteTask(id) {
        if (this.props.usersId.length > 0) {
            this.props.onDeleteTask(this.props.usersId, id)
        } else {
            this.props.onDeleteTask(this.props.u.owner_id, id)
        }
    }

    onDeleteState(id) {
        if (this.props.usersId.length > 0) {
            this.props.onDeleteState(this.props.usersId, id)
        } else {
            this.props.onDeleteState(this.props.u.owner_id, id)
        }
    }

    onManager(ev) {
        if (this.props.usersId.length > 0) {
            this.props.onChangeManager(this.props.usersId, ev.target.value);
        } else {
            this.props.onChangeManager(this.props.u.owner_id, ev.target.value);
        }
        ev.target.value = '';
    }

    onChecked(ev) {
        this.setState({checked: !this.state.checked})
        if (ev.target.value === 'false') {
            this.props.onSelectUser(this.props.u.owner_id)
        } else {
            this.props.onDeselectUser(this.props.u.owner_id)
        }
    }

    addPlanedPair(ev) {
        ev.preventDefault();
        this.props.onAddPlanedPair(this.state.plannedBreed, this.state.plannedDate, this.props.u.owner_id);
        this.setState({showAddPlanned: !this.state.showAddPlanned});
        this.setState({plannedBreed: '', plannedDate: ''})
    }

    render() {
        const {u} = this.props;
        let countryId;
        if(u.country === 'Ukraine') {
            countryId = 1;
        } else if (u.country === 'Russia') {
            countryId = 2;
        } else {
            countryId = 3;
        }
        let breedsList = [];
        let pairsLength = 0;
        function contains(a) {
            for (var i = 0; i < a.length; i++) {
                if (a[i].title === "Новый") {
                    return true;
                }
            }
            return false;
        }
        let newUser = contains(this.props.u.controls[0].status || []);
        u.favorites.forEach(b => {
            breedsList.push(b.title);
            pairsLength = pairsLength + b.pairs.length
        });
        const pairs = u.favorites.map((f, idx) => (
            <PairItem onAddFavoriteBreedPair={this.props.onAddFavoriteBreedPair} owner_id={this.props.u.owner_id} key={idx}
                      f={f} onDeleteKin={this.props.onDeleteKin} onDeletePair={this.props.onDeletePair}
                      onEditPair={this.props.onEditPair} onAddChild={this.props.onAddChild}/>));
        const phones = u.mobile.split(';');
        const planned = u.planned.map((planned, idx) => <tr key={idx}>
            <th colSpan={2} style={{fontWeight: 'bold', overflow: 'hidden', width: '60%'}}>{planned.title}</th>
            <th colSpan={3}>{planned.birthday}</th>
            <th colSpan={1} className="icon-wrap"><span className="delete-icon" onClick={() => this.props.onDeletePlannedPair(planned.id)}> </span>
            </th>
        </tr>)
        return (
            <tr className="user-item" style={{background: (newUser ? '#F0E68C': 'inherit')}}>
                <th className="input" scope="col" style={{textAlign: 'center'}}><input style={{fontSize: '16px', transform: 'scale(1.2)'}}
                                                         type="checkbox" onChange={this.onChecked}
                                                         value={this.state.checked}
                                                         checked={this.props.usersId.indexOf(u.owner_id) === -1 ? false : true}/>
                </th>
                <th className="state" scope="col">
                    <select className="form-control" defaultValue={u.controls[0].status.title}
                            onChange={(ev) => this.onState(ev)}>
                        <option value="">Статус</option>
                        {this.props.stateList.map((s, idx) => <option key={idx} value={s.id}>{s.title}</option>)}
                    </select>
                    {u.controls[0].status.map((status, idx) => <p className="status-item" key={idx}>{status.title}<span
                        className="btn btn-secondary" onClick={(ev) => this.onDeleteState(status.condition)}>X</span>
                    </p>)}
                </th>
                <th className="manager" scope="col">
                    {this.props.rank === 0 || this.props.rank === 1 ? (
                            <select className="form-control" onChange={(ev) => this.onManager(ev)}>
                                <option value="">Менеджер</option>
                                {this.props.managersList.map((m, idx) => <option key={idx} value={m.id}>{m.name}</option>)}
                            </select>)
                        :
                        ''
                    }
                    <span>{u.controls[0].manager.name}</span>
                </th>
                <th className="task" scope="col">
                    <div className="task-wrap">
                        <div className="task-calendar-wrap" style={{flexBasis: '80%'}}>
                            <select className="form-control" value={this.state.task} onChange={this.onTask}>
                                <option value="">Задание</option>
                                {this.props.tasksList.map((t, idx) => <option key={idx}
                                                                              value={t.id}>{t.title}</option>)}
                            </select>
                            <Calendar date={this.state.date} handleFromChange={this.handleFromChange}/>
                        </div>
                        <button className="btn btn-primary grey" onClick={this.onClick}>Добавить</button>
                    </div>
                    {u.controls[0].task.map((task, idx) => <p className="task-item"
                                                          key={idx}>{task.title}{task.deadline ? ' - ' : ''}{task.deadline ?
                        <span className="task-deadline" style={{marginRight: '5px'}}>{task.deadline}</span> : ''}<span
                        className="btn btn-secondary delete-task"
                        onClick={(ev) => this.onDeleteTask(task.mission)}>X</span></p>)}
                </th>
                <th className="company" scope="col" style={{position: 'relative'}}>
                    {u.company ? <p>{u.company}</p> : ''}
                    <p style={{display: 'flex'}}><span className="u-name-icon"> </span> {u.first_name + " " + u.last_name}</p>
                    <p style={{display: 'flex'}}><span className="u-location-icon"> </span> {u.country + " " + u.city}
                    </p>
                    <p style={{display: 'flex'}}><span className="u-calendar-icon"> </span>{u.data_joined}</p>
                    <div><span className="up_down"> </span><Calendar date={u.controls[0].called == null ? '' : u.controls[0].called} handleFromChange={this.lastCallChange} placeholder="Дата последнего звонка"/></div>
                    <p onDoubleClick={() => this.setState({changeComment: !this.state.changeComment})}
                       style={{display: 'flex'}}><span className="u-comment-icon" style={{paddingRight: '16px'}}> </span>{this.state.changeComment ?
                        <textarea maxLength={100} onKeyPress={(ev) => {
							if (ev.key === 'Enter') {
								this.props.onChangeComment(u.owner_id, this.comment.value);
								this.setState({changeComment: false})
							}
						}} placeholder='Комментарий' defaultValue={u.controls[0].comment} ref={text => this.comment = text}></textarea> : u.controls[0].comment ?
                            <span style={{wordBreak: 'break-word'}}>{u.controls[0].comment}</span> : 'Комментарий'}</p>
                    {this.state.showEdit ?
                        <EditProfile onEditProfile={this.props.onEditProfile} countryId={countryId} cities={this.props.cities}
                                     getCitiesList={this.props.getCitiesList} person={u}
                                     onShowEdit={this.onShowEdit}/> : ''}
                    <button onClick={this.onShowEdit} className="btn btn-primary grey">Редактировать</button>
                     <a  className="btn btn-primary grey" style={{marginTop: '5px'}} title='Войти' href={`http://user.pet4u.com.ua/account/token=${u.token}`}target="_blank">Войти</a>
                </th>
                <th className="contacts" scope="col">
                    <p style={{display: 'flex'}}><span className="u-phone-icon"> </span> {phones[0]}</p>
                    {phones[1] ? (<p><span className="u-phone-icon"> </span> {phones[1]}</p>) : ''}
                    <p style={{display: 'flex', wordBreak: 'break-word'}}><span className="u-email-icon" style={{paddingRight: '15px'}}> </span>
                        <a style={{wordBreak: 'break-word', textDecoration: 'none', color: 'black'}}
                           href={`mailto:${u.email}`}>{u.email}</a></p>
                    <div className="social-wrap">
                        <span className={u.feedback.viber ? 'viber' : 'viber-false'}> </span>
                        <span className={u.feedback.whatsapp ? 'whats' : 'whats-false'}> </span>
                        <span className={u.feedback.telegram ? 'telegram' : 'telegram-false'}> </span>
                        {u.facebook ? <a target="_blank" href={u.facebook} className="facebook"> </a> :
                            <span className="facebook-false"> </span>}
                        {u.social ? <a target="_blank" href={u.social} className="social-link"> </a> :
                            <span className="social-link-false"> </span>}
                    </div>
                </th>
                <th className="breeds-u" scope="col" style={{position: 'relative'}}>
                    {breedsList.map((b, idx) => <p key={idx} style={{paddingRight: '16px'}}>{b}</p>)}
                    <button onClick={this.onShowAddBreed} className="btn btn-primary grey">Добавить породу</button>
                    {this.state.showAddNewBreed ? (
                        <div className="modal" role="dialog" aria-labelledby="exampleModalCenterTitle"
                             aria-hidden="true">
                            <div className="modal-dialog modal-dialog-centered" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title">Добавить породу</h5>
                                        <button onClick={this.onShowAddBreed} type="button" className="close"
                                                data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                    </div>
                                    <div className="modal-body">
                                        <div className="form-group row">
                                            <div className="col-sm-12">
                                                <input style={{display: 'inline-block'}} className="form-control"
                                                       placeholder="Название породы" type="text"
                                                       value={this.state.breed} onChange={this.onAddBreedChange}/>
                                                <ul className="addBreedList list-group">
                                                    {this.props.breedsList.filter(b => b.name.toLocaleLowerCase().includes(this.state.breed.toLocaleLowerCase())).map((b, idx) =>
                                                        <li onClick={() => this.setState({breed: b.name})}
                                                            className="list-group-item" key={idx}>{b.name}</li>)}
                                                </ul>
                                            </div>
                                        </div>
                                        <div className="modal-footer">
                                            <button type="button" className="btn btn-primary" data-dismiss="modal"
                                                    onClick={this.onShowAddBreed}>Отмена
                                            </button>
                                            <button type="button" className="btn btn-primary" onClick={(ev) => {
                                                this.props.onAddFavoriteBreed(u.owner_id, this.state.breed);
                                                this.onShowAddBreed(ev);
                                            }}>Добавить
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>) : null}
                </th>
                <th className="pairs-u" scope="col">
                    <span onClick={(ev) => this.onShowBroods(ev)}>Кол-во пометов: {pairsLength} <span
                        style={{display: 'inline-block', fontSize: this.state.show ? '13px' : '12px', color: '#007bff', cursor: 'pointer'}}>{this.state.show ? '▲' : '▼'}</span></span>
                    <table className={'broods ' + (this.state.show ? '' : 'hidden')}>
                        <tbody>
                        <tr>
                            <th colSpan={4}>{pairs}</th>
                        </tr>
                        <tr>
                            <th colSpan={4}>Запланированные
                                <button className="btn btn-secondary"
                                        onClick={() => this.setState({showAddPlanned: !this.state.showAddPlanned})}>+
                                </button>
                                {this.state.showAddPlanned ? (
                                    <div className="modal" role="dialog" aria-labelledby="exampleModalCenterTitle"
                                         aria-hidden="true">
                                        <div className="modal-dialog modal-dialog-centered" role="document">
                                            <div className="modal-content">
                                                <div className="modal-header">
                                                    <h5 className="modal-title">Добавить запланированный помет</h5>
                                                    <button
                                                        onClick={() => this.setState({showAddPlanned: !this.state.showAddPlanned})}
                                                        type="button" className="close" data-dismiss="modal"
                                                        aria-label="Close"><span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div className="modal-body">
                                                    <div className="form-group row">
                                                        <div className="col-sm-12">
                                                            <Calendar style={{display: 'inline-block'}}
                                                                      date={this.state.plannedDate}
                                                                      handleFromChange={this.onDateChange}/>
                                                            <input style={{display: 'inline-block', width: '280px'}}
                                                                   className="form-control"
                                                                   placeholder="Название породы" type="text"
                                                                   value={this.state.plannedBreed}
                                                                   onChange={this.onPlannedBreedChange}/>
                                                            {this.state.autoBreed.map((b, idx) => (
                                                                <label key={idx} onClick={this.onAuto}
                                                                       className="form-control" style={{
                                                                    textAlign: 'center',
                                                                    height: '33px',
                                                                    width: '450px'
                                                                }}>{b}</label>))}
                                                        </div>
                                                    </div>
                                                    <div className="modal-footer">
                                                        <button type="button" className="btn btn-primary"
                                                                data-dismiss="modal"
                                                                onClick={() => this.setState({showAddPlanned: !this.state.showAddPlanned})}>Отмена
                                                        </button>
                                                        <button type="button" className="btn btn-primary"
                                                                onClick={this.addPlanedPair}>Добавить
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>) : null} </th>
                        </tr>
                        {planned}
                        </tbody>
                    </table>
                </th>
            </tr>
        )
    }
}
