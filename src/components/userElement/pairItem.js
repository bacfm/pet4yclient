import React, {Component} from 'react';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import {formatDate, parseDate} from 'react-day-picker/moment';

const MONTHS = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];
const WEEKDAYS_LONG = [
    'Воскресенье',
    'Понедельник',
    'Вторник',
    'Среда',
    'Четверг',
    'Пятница',
    'Суббота',
];
const WEEKDAYS_SHORT = ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'];

export default class PairItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: null,
            price: this.props.f.price,
            date: this.props.f.birthday,
            currency: this.props.f.currency,
            dad_feat: '',
            dad_ped: '',
            mom_feat: '',
            mom_ped: '',
            showDel: null,
            showAddPair: false,
            newBirth: '',
            male: '',
            female: '',
            currencyAdd: 'UAH',
            delChild: null
        }
        this.onSave = this.onSave.bind(this);
        this.onShow = this.onShow.bind(this);
        this.onDate = this.onDate.bind(this);
        this.onPrice = this.onPrice.bind(this);
        this.onCurrency = this.onCurrency.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.showDelPopup = this.showDelPopup.bind(this);
        this.closeDel = this.closeDel.bind(this);
        this.onDadFeat = this.onDadFeat.bind(this);
        this.onDadChange = this.onDadChange.bind(this);
        this.onMomFeat = this.onMomFeat.bind(this);
        this.onMomChange = this.onMomChange.bind(this);
        this.onShowAdd = this.onShowAdd.bind(this);
        this.onNewBith = this.onNewBith.bind(this);
        this.onMaleChange = this.onMaleChange.bind(this);
        this.onFemaleChange = this.onFemaleChange.bind(this);
        this.onAddCurrency = this.onAddCurrency.bind(this);
        this.onPrice = this.onPrice.bind(this);
        this.onAddNewPair = this.onAddNewPair.bind(this);
        this.onShowDelChild = this.onShowDelChild.bind(this);
    }

    onMaleChange(ev) {
        ev.preventDefault();
        this.setState({male: ev.target.value})
    }

    onFemaleChange(ev) {
        ev.preventDefault();
        this.setState({female: ev.target.value})
    }

    onDadFeat(ev) {
        ev.preventDefault();
        this.setState({dad_feat: ev.target.value})
    }

    onDadChange(ev) {
        ev.preventDefault();
        this.setState({dad_ped: ev.target.value})
    }

    onMomFeat(ev) {
        ev.preventDefault();
        this.setState({mom_feat: ev.target.value})
    }

    onMomChange(ev) {
        ev.preventDefault();
        this.setState({mom_ped: ev.target.value})
    }

    onShow(idx) {
        this.setState({showModal: idx})
    }

    closeModal() {
        this.setState({showModal: null})
    }

    onDate(day) {
        this.setState({date: day.getFullYear() + '-' + (Number(day.getMonth()) + 1) + '-' + day.getDate()});
    }

    onNewBith(day) {
        this.setState({newBirth: day.getFullYear() + '-' + (Number(day.getMonth()) + 1) + '-' + day.getDate()});
    }

    onPrice(ev) {
        ev.preventDefault();
        this.setState({price: ev.target.value})
    }

    onCurrency(ev) {
        ev.preventDefault();
        this.setState({currency: ev.target.value})
    }

    onAddCurrency(ev) {
        ev.preventDefault();
        this.setState({currencyAdd: ev.target.value})
    }

    onSave(ev, id) {
        ev.preventDefault();
        this.props.onEditPair(id, this.state.price, this.state.date, this.state.currency, this.state.dad_feat, this.state.dad_ped, this.state.mom_feat, this.state.mom_ped);
        this.closeModal();
    }

    showDelPopup(idx) {
        this.setState({showDel: idx})
    }

    closeDel() {
        this.setState({showDel: null})
    }

    onShowAdd(ev) {
        ev.preventDefault();
        this.setState({showAddPair: !this.state.showAddPair})
    }

    onPrice(ev) {
        ev.preventDefault();
        this.setState({price: ev.target.value})
    }
    onShowDelChild(idx){
        this.setState({delChild: idx})
    }

    onAddNewPair(ev) {
        ev.preventDefault();
        this.props.onAddFavoriteBreedPair(this.props.owner_id, this.state.newBirth, this.state.currencyAdd, this.props.f.title, this.state.price, this.state.male, this.state.female, this.props.f.id);
        this.onShowAdd(ev)
        this.setState({
            newBirth: '',
            currencyAdd: '',
            price: '',
            male: '',
            female: ''
        })
    }

    render() {
        const {f} = this.props;
        return (
            <table className="pairs">
                <tbody>
                <tr>
                    <th colSpan={8} style={{fontWeight: 'bold'}}>{f.title}
                        <button className="btn btn-secondary" onClick={this.onShowAdd}>+</button>
                        {this.state.showAddPair ? (
                            <div className="modal" role="dialog" aria-labelledby="exampleModalCenterTitle"
                                 aria-hidden="true">
                                <div className="modal-dialog modal-dialog-centered" role="document">
                                    <div className="modal-content">
                                        <div className="modal-header">
                                            <h5 className="modal-title">Добавить помет {f.title}</h5>
                                            <button onClick={this.onShowAdd} type="button" className="close"
                                                    data-dismiss="modal" aria-label="Close"><span
                                                aria-hidden="true">&times;</span></button>
                                        </div>
                                        <div className="modal-body">
                                            <div className="form-group row">
                                                <div className="col-sm-12"
                                                     style={{display: 'flex', justifyContent: 'center'}}>
                                                    <DayPickerInput
                                                        value={this.state.newBirth}
                                                        formatDate={formatDate}
                                                        parseDate={parseDate}
                                                        format="LL"
                                                        dayPickerProps={{
                                                            locale: "ru",
                                                            months: MONTHS,
                                                            weekdaysLong: WEEKDAYS_LONG,
                                                            weekdaysShort: WEEKDAYS_SHORT,
                                                        }}
                                                        placeholder="Выберите дату"
                                                        onDayChange={this.onNewBith}
                                                    />
                                                </div>
                                            </div>
                                            <div className="form-group row">
                                                <div className="col-sm-12"
                                                     style={{display: 'flex', justifyContent: 'center'}}>
                                                    <label style={{marginLeft: '5px'}}>Цена:</label>
                                                    <input className="form-control" value={this.state.price}
                                                           onChange={this.onPrice} style={{
                                                        textAlign: 'center',
                                                        width: '45px',
                                                        display: 'inline-block',
                                                        marginLeft: '2px',
                                                        padding: '0'
                                                    }} type="text"/>
                                                    <select style={{
                                                        width: '65px',
                                                        display: 'inline-block',
                                                        marginLeft: '15px',
                                                        padding: '0'
                                                    }} className="form-control" defaultValue={this.state.currencyAdd}
                                                            onChange={this.onAddCurrency}>
                                                        <option value="UAH">UAH</option>
                                                        <option value="RUB">RUB</option>
                                                        <option value='USD'>USD</option>
                                                        <option value='EUR'>EUR</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div className="form-group row">
                                                <div className="col-sm-12"
                                                     style={{display: 'flex', justifyContent: 'center'}}>
                                                    <label style={{marginLeft: '5px'}}>Male:</label>
                                                    <input className="form-control" value={this.state.male}
                                                           onChange={this.onMaleChange} style={{
                                                        textAlign: 'center',
                                                        width: '35px',
                                                        display: 'inline-block',
                                                        marginLeft: '2px',
                                                        padding: '0'
                                                    }} type="text"/>
                                                    <label style={{marginLeft: '5px'}}>Female:</label>
                                                    <input className="form-control" value={this.state.female}
                                                           onChange={this.onFemaleChange} style={{
                                                        textAlign: 'center',
                                                        width: '35px',
                                                        display: 'inline-block',
                                                        marginLeft: '2px',
                                                        padding: '0'
                                                    }} type="text"/>
                                                </div>
                                            </div>
                                            <div className="modal-footer">
                                                <button type="button" className="btn btn-primary" data-dismiss="modal"
                                                        onClick={this.onShowAdd}>Отмена
                                                </button>
                                                <button type="button" className="btn btn-primary"
                                                        onClick={this.onAddNewPair}>Добавить
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>) : null}
                    </th>
                </tr>
                {f.pairs.map((p, idx) => {
                    let girls = [];
                    let boys = [];
                    p.children.map(c => {
                        if (c.sex === "M") {
                            boys.push(c);
                        } else {
                            girls.push(c);
                        }
                    })
                    return <tr key={idx}>
                        <td colSpan={3}>{p.birthday.slice(2).replace(/-/g, ".")}</td>
                        <td colSpan={1}>M-{p.boys}</td>
                        <td colSpan={1}>F-{p.girls}</td>
                        <td colSpan={1} className="icon-wrap"><span className="edit-icon"
                                                                    onClick={() => this.onShow(idx)}></span></td>
                        <td colSpan={1} className="icon-wrap"><span className="delete-icon"
                                                                    onClick={() => this.showDelPopup(idx)}></span></td>
                        <th colSpan={1} className="icon-wrap"><a className="down-icon"
                                                                 href={`http://api.pet4u.com.ua/api/v1/brood/zip?id=${p.id}`}
                                                                 target="_blank"></a></th>
                        <th>
                            {this.state.showDel === idx ? (
                                    <div className="modal" role="dialog" aria-labelledby="exampleModalCenterTitle"
                                         aria-hidden="true">
                                        <div className="modal-dialog modal-dialog-centered" role="document">
                                            <div className="modal-content">
                                                <div className="modal-header">
                                                    <h4 className="modal-title">Вы уверены что хотите удалить помет?</h4>
                                                </div>
                                                <div className="modal-footer">
                                                    <button className="btn btn-primary"
                                                            onClick={() => this.closeDel()}>Отмена
                                                    </button>
                                                    <button className="btn btn-danger" onClick={(ev) => {
                                                        this.props.onDeletePair(p.id);
                                                        this.closeDel()
                                                    }}>Удалить
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                )
                                :
                                ''
                            }
                            {this.state.showModal === idx ? (
                                    <div className="modal" role="dialog" aria-labelledby="exampleModalCenterTitle"
                                         aria-hidden="true">
                                        <div className="modal-dialog modal-dialog-centered" role="document">
                                            <div className="modal-content">
                                                <div className="modal-body">
                                                    <button type="button" className="close"
                                                            onClick={() => this.closeModal()} data-dismiss="modal"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                    <div className="form-group row">
                                                        <label htmlFor="staticEmail"
                                                               className="col-sm-2 col-form-label">Цена</label>
                                                        <div className="col-sm-10">
                                                            <input style={{width: '80px', display: 'inline-block'}}
                                                                   type="text" className="form-control"
                                                                   defaultValue={p.cost} onChange={this.onPrice}/>
                                                            <select style={{
                                                                width: '65px',
                                                                display: 'inline-block',
                                                                marginLeft: '15px'
                                                            }} className="form-control"
                                                                    defaultValue={p.currency} onChange={this.onCurrency}>
                                                                <option value="UAH">UAH</option>
                                                                <option value="RUB">RUB</option>
                                                                <option value='USD'>USD</option>
                                                                <option value='EUR'>EUR</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div className="form-group row">
                                                        <label htmlFor="staticEmail"
                                                               className="col-sm-2 col-form-label">Дата</label>
                                                        <div className="col-sm-10">
                                                            <DayPickerInput
                                                                value={p.birthday}
                                                                formatDate={formatDate}
                                                                parseDate={parseDate}
                                                                format="LL"
                                                                dayPickerProps={{
                                                                    locale: "ru",
                                                                    months: MONTHS,
                                                                    weekdaysLong: WEEKDAYS_LONG,
                                                                    weekdaysShort: WEEKDAYS_SHORT,
                                                                }}
                                                                placeholder="Выберите дату"
                                                                onDayChange={this.onDate}
                                                            />
                                                        </div>
                                                    </div>
                                                    {p.parents.father ? (<div className="form-group row">
                                                            <div className="col-sm-12">
                                                                <span>Папа</span>
                                                                <input className="form-control" onChange={this.onDadFeat}
                                                                       defaultValue={p.parents.father.feat}
                                                                       placeholder="Достижения"/>
                                                                <input className="form-control" onChange={this.onDadChange}
                                                                       defaultValue={p.parents.father.pedigree}
                                                                       placeholder="Ссылка на родословную"/>
                                                            </div>
                                                        </div>)
                                                        :
                                                        ""
                                                    }
                                                    {p.parents.mother ? (<div className="form-group row">
                                                            <div className="col-sm-12">
                                                                <span>Мама</span>
                                                                <input className="form-control" onChange={this.onMomFeat}
                                                                       defaultValue={p.parents.mother.feat}
                                                                       placeholder="Достижения"/>
                                                                <input className="form-control" onChange={this.onMomChange}
                                                                       defaultValue={p.parents.mother.pedigree}
                                                                       placeholder="Ссылка на родословную"/>
                                                            </div>
                                                        </div>)
                                                        :
                                                        ""
                                                    }
                                                    <div className="form-group row">
                                                        <label htmlFor="staticEmail"
                                                               className="col-sm-4 col-form-label">Девочек: {p.girls}
                                                            <button disabled={p.girls >= 15 ? true : false}
                                                                    onClick={() => this.props.onAddChild(p.id, 'girl')}
                                                                    className='btn btn-secondary'>+
                                                            </button>
                                                        </label>
                                                        <div className="col-sm-12">
                                                            {girls.map((g, idx) => (
                                                                <span className="child-item" key={idx}>F:{g.num}
                                                                    <button onClick={() => this.onShowDelChild(g.id)}
                                                                            className="btn btn-secondary delete-child">X</button>
                                                                    {this.state.delChild === g.id ? (<div className="modal" role="dialog" aria-labelledby="exampleModalCenterTitle"
                                                                                                          aria-hidden="true">
                                                                        <div className="modal-dialog modal-dialog-centered" role="document">
                                                                            <div className="modal-content" style={{margin: '0 auto', width: '60%'}}>
                                                                                <div className="modal-header">
                                                                                    <h4 className="modal-title">Вы уверены что хотите удалить?</h4>
                                                                                </div>
                                                                                <div className="modal-footer">
                                                                                    <button className="btn btn-primary"
                                                                                            onClick={() => this.setState({delChild: null})}>Отмена
                                                                                    </button>
                                                                                    <button className="btn btn-danger" onClick={() => {this.props.onDeleteKin(g.id); this.setState({delChild: null})}}>Удалить
                                                                                    </button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>): null}
                                                                </span>))}
                                                        </div>
                                                    </div>
                                                    <div className="form-group row">
                                                        <label htmlFor="staticEmail"
                                                               className="col-sm-4 col-form-label">Мальчиков: {p.boys}
                                                            <button disabled={p.boys >= 15 ? true : false}
                                                                    onClick={() => this.props.onAddChild(p.id, 'boy')}
                                                                    className='btn btn-secondary'>+
                                                            </button>
                                                        </label>
                                                        <div className="col-sm-12">
                                                            {boys.map((b, idx) => (
                                                                <span className="child-item" key={idx}>M:{b.num}
                                                                    <button onClick={() => this.onShowDelChild(b.id)}
                                                                            className="btn btn-secondary delete-child">X</button>
                                                                    {this.state.delChild === b.id ? (<div className="modal" role="dialog" aria-labelledby="exampleModalCenterTitle"
                                                                                                          aria-hidden="true">
                                                                        <div className="modal-dialog modal-dialog-centered" role="document">
                                                                            <div className="modal-content" style={{margin: '0 auto', width: '60%'}}>
                                                                                <div className="modal-header">
                                                                                    <h4 className="modal-title">Вы уверены что хотите удалить?</h4>
                                                                                </div>
                                                                                <div className="modal-footer">
                                                                                    <button className="btn btn-primary"
                                                                                            onClick={() => this.setState({delChild: null})}>Отмена
                                                                                    </button>
                                                                                    <button className="btn btn-danger" onClick={() => {this.props.onDeleteKin(b.id); this.setState({delChild: null})}}>Удалить
                                                                                    </button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>): null}
                                                                </span>))}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="modal-footer">
                                                    <button type="button" className="btn btn-primary" data-dismiss="modal"
                                                            onClick={() => this.closeModal()}>Закрыть
                                                    </button>
                                                    <button type="button" className="btn btn-primary"
                                                            onClick={(ev) => this.onSave(ev, p.id)}>Сохранить
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>)
                                :
                                ''
                            }
                        </th>
                    </tr>
                })}
                </tbody>
            </table>
        );
    }
}
