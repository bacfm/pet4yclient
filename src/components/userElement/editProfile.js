import React, { Component } from 'react';


export default class EditProfile extends Component {
    constructor(props){
        super(props);
        this.state = {
            first_name: '',
            last_name: '',
            company: '',
            description: '',
            facebook: '',
            country: '',
            mobile: '',
            viber: this.props.person.feedback.viber,
            whats: this.props.person.feedback.whatsapp,
            telegram: this.props.person.feedback.telegram,
            social: this.props.person.feedback.social
        }
        this.onCitiesChange = this.onCitiesChange.bind(this);
        this.onClick = this.onClick.bind(this);
        this.onSoc = this.onSoc.bind(this);
        this.onTelegram = this.onTelegram.bind(this);
        this.onWhat = this.onWhat.bind(this);
        this.onViber = this.onViber.bind(this);
    }
    onViber(ev){
        this.setState({ viber: !this.state.viber })
    }
    onWhat(ev){
        this.setState({whats: !this.state.whats})
    }
    onTelegram(ev){
        this.setState({ telegram: !this.state.telegram })
    }
    onSoc(ev){
        this.setState({ social: !this.state.social })
    }
    componentWillMount(){
        this.props.getCitiesList(this.props.countryId);
    }
    onCitiesChange(ev){
        this.props.getCitiesList(ev.target.value)
    }
    onClick(ev){
        ev.preventDefault();
        let mobile = this.phone.value + ';' + this.sphone.value;
        this.props.onEditProfile(this.props.person.owner_id, this.first_name.value, this.last_name.value, this.company.value, this.description.value, this.facebook.value, this.country.value, this.city.value, this.state.viber, this.state.whats, this.state.telegram, this.state.social, mobile, this.instagram.value)
        this.props.onShowEdit(ev);
    }
    render(){
        const { person } = this.props;
        const personCountryNum = person.country === 'Russia' ? 2 : person.country === 'Ukraine' ? 1 : 3;
        return (
            <div className="modal" role="dialog" aria-labelledby="exampleModalCenterTitle"
                 aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Редактирование профиля</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.props.onShowEdit}>
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <div className="form-group row">
                                <label className="col-sm-3 col-form-label">Имя</label>
                                <div className="col-sm-9">
                                    <input  className="form-control" type="text" placeholder="Имя" defaultValue={person.first_name} ref={(input) => this.first_name = input}/>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-sm-3 col-form-label">Фамилия</label>
                                <div className="col-sm-9">
                                    <input className="form-control" type="text" placeholder="Фамилия" defaultValue={person.last_name} ref={(input) => this.last_name = input}/>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-sm-3 col-form-label">Питомник</label>
                                <div className="col-sm-9">
                                    <input className="form-control" type="text" placeholder="Питомник" defaultValue={person.company} ref={(input) => this.company = input}/>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-sm-3 col-form-label">Описание</label>
                                <div className="col-sm-9">
                                    <input className="form-control" type="text" placeholder="Описание" defaultValue={person.description} ref={(input) => this.description = input}/>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-sm-3 col-form-label">Facebook</label>
                                <div className="col-sm-9">
                                    <input className="form-control" type="text" placeholder="Описание" defaultValue={person.facebook} ref={(input) => this.facebook = input}/>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-sm-3 col-form-label">Instagram</label>
                                <div className="col-sm-9">
                                    <input className="form-control" type="text" placeholder="Описание" defaultValue={person.social} ref={(input) => this.instagram = input}/>
                                </div>
                            </div>
                            <div className="form-group row" style={{justifyContent: 'center'}}>
                                <select style={{width: '40%'}} className="form-control" defaultValue={personCountryNum} onChange={this.onCitiesChange} ref={(select) => {this.country = select}}>
                                    <option value="1">Украина</option>
                                    <option value="2">Россия</option>
                                    <option value="3">Беларусь</option>
                                </select>
                                <select  style={{width: '40%'}} className="form-control" defaultValue={person.city} ref={(input) => this.city = input}>
                                    {this.props.cities.map((c, idx) => <option key={idx} value={c.name}>{c.name}</option>)}
                                </select>
                            </div>
                            <div className="form-group row">
                                <label className="col-sm-3 col-form-label">Телефон</label>
                                <div className="col-sm-9">
                                    <input className="form-control" type="text" placeholder="Телефон" defaultValue={person.mobile.split(';')[0]} ref={(input) => this.phone = input}/>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-sm-3 col-form-label">Телефон</label>
                                <div className="col-sm-9">
                                    <input className="form-control" type="text" placeholder="Телефон" defaultValue={person.mobile.split(':')[1] || ''} ref={(input) => this.sphone = input}/>
                                </div>
                            </div>
                            <div className="form-check form-check-inline">
                                <input style={{marginTop: '6px'}} className="form-check-input" type="checkbox" defaultValue={this.state.viber} onChange={this.onViber} defaultChecked={this.state.viber}/>
                                    <label className="form-check-label"  >Viber</label>
                            </div>
                            <div className="form-check form-check-inline">
                                <input style={{marginTop: '6px'}} className="form-check-input" type="checkbox" defaultValue={this.state.whats} onChange={this.onWhat} defaultChecked={this.state.whats}/>
                                <label className="form-check-label" >WhatsApp</label>
                            </div>
                            <div className="form-check form-check-inline">
                                <input style={{marginTop: '6px'}} className="form-check-input" type="checkbox" defaultValue={this.state.telegram} defaultChecked={this.state.telegram} onChange={this.onTelegram} />
                                <label className="form-check-label">Telegram</label>
                            </div>
                            <div className="form-check form-check-inline">
                                <input style={{marginTop: '6px'}} className="form-check-input" type="checkbox" defaultValue={this.state.social} defaultChecked={this.state.social} onChange={this.onSoc}/>
                                <label className="form-check-label" >Instagram</label>
                            </div>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-primary" data-dismiss="modal" onClick={this.props.onShowEdit}>Закрыть</button>
                            <button type="button" className="btn btn-primary" onClick={this.onClick}>Сохранить</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
