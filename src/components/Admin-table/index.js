import React, { Component } from 'react';
import { connect } from 'react-redux';
import Pagination from 'rc-pagination';
import 'rc-pagination/assets/index.css';
import {
    getUsers,
    getFilteredUsersInfo,
    getBreeds,
    deletePair,
    editPair,
    getAllUsersList,
    rankChange,
    addChild,
    getLogs
} from '../../api';
import {selectUser, deselectUser, cleanSelectedUsers} from '../../actions';
import AdminControlls from './admin-controlls';
import TableElement from '../userElement';
import Filters from '../Filters';
import './style.css';
import {
    getCities,
    addTask,
    onStateChange,
    deleteTask,
    changeManager,
    deleteState,
    deleteKin,
    editUserProfile,
    addPlannedPair,
    deletePlannedPair,
    addFavoriteBreed,
    addFavoriteBreedPair,
    changeUserComment,
    onLastCallChange,
    regUser,
	getUserRank
} from "../../api/index";
import RegForm from './regForm';
import Logs from './logs';


class AdminTable extends Component {
    constructor(props){
        super(props);
        this.state = {
            filters: false,
            filterDate: '',
            city: '',
            country: '',
            manager: '',
            task: '',
            personName: '',
            breedFilter: '',
            breed: '',
            email: '',
            company: '',
            state: '',
            phone: '',
            planned: false,
            male: false,
            female: false,
            profileChoose: 1,
            displayForAdmin: false,
            idList: [],
            selectAll: false,
            viber: false,
            whatsapp: false,
            facebook: false,
            telegram: false,
            anotherSocial: false,
            showRegForm: false,
            showLogs: false,
            dog_name_key: '',
            currentPage: window.localStorage.page || 1,
			useFilters: false
        }
        this.onShowRegForm = this.onShowRegForm.bind(this);
        this.onFilterDateChange = this.onFilterDateChange.bind(this);
        this.onDisplayFilter = this.onDisplayFilter.bind(this);
        this.onShowFilters = this.onShowFilters.bind(this);
        this.onEmail = this.onEmail.bind(this);
        this.onCompany = this.onCompany.bind(this);
        this.onName = this.onName.bind(this);
        this.onCountry = this.onCountry.bind(this);
        this.onCity = this.onCity.bind(this);
        this.onFilter = this.onFilter.bind(this);
        this.onManager = this.onManager.bind(this);
        this.onBreedFilter = this.onBreedFilter.bind(this);
        this.onState = this.onState.bind(this);
        this.onBreed = this.onBreed.bind(this);
        this.onSetBreed = this.onSetBreed.bind(this);
        this.onTask = this.onTask.bind(this);
        this.onPhone = this.onPhone.bind(this);
        this.onPlanned = this.onPlanned.bind(this);
        this.onMale = this.onMale.bind(this);
        this.onFemale = this.onFemale.bind(this);
        this.onPageChange = this.onPageChange.bind(this);
        this.onChooseProfile = this.onChooseProfile.bind(this);
        this.onAdminControllsDisplay = this.onAdminControllsDisplay.bind(this);
        this.onReset = this.onReset.bind(this);
        this.onSelectAll = this.onSelectAll.bind(this)
        this.onViber = this.onViber.bind(this);
        this.onWhats = this.onWhats.bind(this);
        this.onFacebook = this.onFacebook.bind(this);
        this.onTelegram = this.onTelegram.bind(this);
        this.onAnotherSoc = this.onAnotherSoc.bind(this);
        this.onLogsShow = this.onLogsShow.bind(this);
        this.onDogKey = this.onDogKey.bind(this);
        this.onHandlePage = this.onHandlePage.bind(this);
    }
    componentWillMount(){
        if (!window.localStorage.page) {
            window.localStorage.setItem('page', 1);
        }
        this.props.getUsersInf(window.localStorage.page);
        this.props.getRank();
        this.props.log();
    }
    onLogsShow(){
        this.setState({showLogs: !this.state.showLogs})
    }
    onFilterDateChange(data){
        this.setState({filterDate: data.getFullYear() + '-' + (Number(data.getMonth()) + 1) + '-' + data.getDate()})
    }
    onDogKey(ev){
        this.setState({dog_name_key: ev.target.value})
    }
    onShowRegForm(ev){
        ev.preventDefault();
        this.setState({showRegForm: !this.state.showRegForm})
    }
    onViber(ev){
        this.setState({viber: !this.state.viber })
    }
    onWhats(ev){
        this.setState({whatsapp: !this.state.whatsapp})
    }
    onFacebook(ev){
        this.setState({facebook: !this.state.facebook})
    }
    onTelegram(ev){
        this.setState({ telegram: !this.state.telegram })
    }
    onAnotherSoc(ev){
        this.setState({ anotherSocial: !this.state.anotherSocial })
    }
    onPageChange(num){
        this.setState({page: num});
        this.props.getUsersInf(num);
    }
    onDisplayFilter(ev){
        ev.preventDefault();
        this.setState({filters: !this.state.filters})
    }
    onShowFilters(ev){
        ev.preventDefault();
        this.setState({ filters: !this.state.filters })
    }
    onEmail(ev){
        ev.preventDefault();
        this.setState({ email: ev.target.value })
    }
    onCompany(ev){
        ev.preventDefault();
        this.setState({ company: ev.target.value })
    }
    onName(ev){
        ev.preventDefault();
        this.setState({ personName: ev.target.value })
    }
    onCountry(ev){
        ev.preventDefault();
        this.props.getCitiesList(ev.target.value);
        this.setState({ country: ev.target.value })
    }
    onCity(ev){
        ev.preventDefault();
        this.setState({ city: ev.target.value })
    }
    onManager(ev){
        ev.preventDefault();
        this.setState({ manager: ev.target.value })
    }
    onChooseProfile(ev, num){
        window.localStorage.setItem('pet_prof', num);
        this.onFilter(ev);
        this.props.getBreedsList();
        this.setState({selectAll: false});
        this.props.cleanUsersListId();
    }
    onBreedFilter(ev){
        ev.preventDefault();
        this.setState({ breedFilter: ev.target.value })
    }
    onBreed(id){
        this.setState({ breed: id })
    }
    onSetBreed(value){
        this.setState({breedFilter: value})
    }
    onState(ev){
        ev.preventDefault();
        this.setState({ state: ev.target.value })
    }
    onTask(ev){
        ev.preventDefault();
        this.setState({ task: ev.target.value })
    }
    onPhone(ev){
        ev.preventDefault();
        this.setState({phone: ev.target.value})
    }
    onPlanned(ev){
        this.setState({ planned: !this.state.planned })
    }
    onMale(ev){
        this.setState({ male: !this.state.male })
    }
    onFemale(ev){
        this.setState({ female: !this.state.female })
    }
    onAdminControllsDisplay(ev){
        this.setState({displayForAdmin: !this.state.displayForAdmin})
    }
    onSelectAll(ev){
        if(ev.target.value === 'false'){
            this.setState({selectAll: true});
            this.props.users.users.forEach(u => this.props.onSelectUser(u.id));
        }else{
            this.setState({selectAll: false})
            this.props.cleanUsersListId();
        }
    }
    onHandlePage(page) {
		this.state.useFilters ?
			this.props.getFiltered(page, this.state.personName, this.state.company, this.state.email, this.state.city, this.state.country, this.state.manager, this.state.breed, this.state.state, this.state.task, this.state.phone, this.state.planned, this.state.male, this.state.female, this.state.whatsapp, this.state.viber, this.state.telegram, this.state.facebook, this.state.anotherSocial, this.state.filterDate, this.state.dog_name_key)
		    : this.props.getUsersInf(page);
        this.setState({currentPage: page});
        window.localStorage.setItem('page', page);
    }
    onReset(ev){
        ev.preventDefault();
        this.props.getFiltered(1, '', '', '', '', '', '', '', '', '', '', false, false, false, false, false, false, false, false, '', '')
        this.setState({
            personName: '',
            company: '',
            email: '',
            city: '',
            country: '',
            manager: '',
            state: '',
            breedFilter: '',
            breed: '',
            task: '',
            phone: '',
            planned: false,
            male: false,
            female: false,
            filters: false,
            viber: false,
            whatsapp: false,
            facebook: false,
            telegram: false,
            anotherSocial: false,
            filterDate: '',
            dog_name_key: '',
            useFilters: false,
            page: 1
        });
        window.localStorage.setItem('page', 1);
    }
    onFilter(ev){
        ev.preventDefault();
        this.setState({page: 1, useFilters: true});
		window.localStorage.setItem('page', 1);
        this.props.getFiltered(1, this.state.personName, this.state.company, this.state.email, this.state.city, this.state.country, this.state.manager, this.state.breed, this.state.state, this.state.task, this.state.phone, this.state.planned, this.state.male, this.state.female, this.state.whatsapp, this.state.viber, this.state.telegram, this.state.facebook, this.state.anotherSocial, this.state.filterDate, this.state.dog_name_key);
    }
  render() {
      let usersInf;
      if (this.props.users && this.props.managers && this.props.states && this.props.cities && this.props.tasks) {
          const {
              tasks,
              onAddTask,
              states,
              onChangeState,
              onDeleteTask,
              managers,
              onChangeManager,
              onSelectUser,
              onDeselectUser,
              usersId,
              onDeletePair,
              onEditPair,
              onDeleteState,
              onDeleteKin,
              getCitiesList,
              cities,
              onEditProfile,
              onAddPlanedPair,
              onDeletePlannedPair,
              breeds,
              onAddFavoriteBreed,
              onAddFavoriteBreedPair,
              onChangeComment,
              onAddChild,
              lastCall,
          } = this.props;
           const userRank = this.props.rank;
           usersInf = this.props.users.map(function (u, idx) {
              return (<TableElement
                  onLastCallChange={lastCall}
                  onAddChild={onAddChild}
                  onChangeComment={onChangeComment}
                  onAddFavoriteBreedPair={onAddFavoriteBreedPair}
                  onAddFavoriteBreed={onAddFavoriteBreed}
                  onDeletePlannedPair={onDeletePlannedPair}
                  onAddPlanedPair={onAddPlanedPair}
                  onEditProfile={onEditProfile}
                  onDeleteKin={onDeleteKin}
                  onDeleteState={onDeleteState}
                  onEditPair={onEditPair}
                  onDeletePair={onDeletePair}
                  usersId={usersId}
                  onDeselectUser={onDeselectUser}
                  onSelectUser={onSelectUser}
                  onChangeManager={onChangeManager}
                  onAddTask={onAddTask}
                  onDeleteTask={onDeleteTask}
                  onChangeState={onChangeState}
                  managersList={managers}
                  stateList={states}
                  tasksList={tasks}
                  rank={userRank}
                  breedsList={breeds}
                  getCitiesList={getCitiesList}
                  cities={cities}
                  u={u} key={idx}/>)

          });

      } return (
              <div className="admin-table">
                  <div className="choose" style={{position: 'fixed', top: '0'}}>
                      <button className={"btn-primary" + ((window.localStorage.pet_prof === '1' || !window.localStorage.pet_prof) ? ' selectedBtn' : '')} disabled={(window.localStorage.pet_prof === '1' || !window.localStorage.pet_prof) ? true : false} onClick={(ev) => this.onChooseProfile(ev, 1)}>Собаки</button>
                      <button className={"btn-primary" + (window.localStorage.pet_prof === '2' ? ' selectedBtn' : '')} disabled={window.localStorage.pet_prof === '2' ? true : false} onClick={(ev) => this.onChooseProfile(ev, 2)}>Кошки</button>
                  </div>
                  <table className="table-custom  table-sm table-hover table-responsive-sm table-fixed">
                      <thead className="thead">
                      <tr>
                          <th className="input"  scope="col" style={{textAlign: 'center'}}><input style={{fontSize: '16px', transform: 'scale(1.2)'}} value={this.state.selectAll} type="checkbox" checked={this.state.selectAll} onChange={this.onSelectAll}/></th>
                          <th className="state" scope="col">Статус</th>
                          <th className="manager" scope="col">Менеджер</th>
                          <th className="task" scope="col">Задание</th>
                          <th className="company" scope="col">Заводчик</th>
                          <th className="contacts" scope="col">Контакты</th>
                          <th className="breeds-u" scope="col">Породы</th>
                          <th className="pairs-u" scope="col">Пометы</th>
                      </tr>
                      </thead>
                      <tbody className="tbody">
                      {usersInf}
                      </tbody>
                  </table>
                  {this.props.managers && this.props.cities && this.props.breeds && this.props.tasks ?
                      (<Filters
                          onDogKey={this.onDogKey}
                          display={this.state.filters}
                          onAnotherSoc={this.onAnotherSoc}
                          onTelegram={this.onTelegram}
                          onFacebook={this.onFacebook}
                          onFilterDateChange={this.onFilterDateChange}
                          onViber={this.onViber}
                          onWhats={this.onWhats}
                          onEmail={this.onEmail}
                          onCompany={this.onCompany}
                          onName={this.onName}
                          onCountry={this.onCountry}
                          onCity={this.onCity}
                          onManager={this.onManager}
                          onBreedFilter={this.onBreedFilter}
                          onBreed={this.onBreed}
                          onFilter={this.onFilter}
                          onState={this.onState}
                          onTask={this.onTask}
                          onSetBreed={this.onSetBreed}
                          onPhone={this.onPhone}
                          onPlanned={this.onPlanned}
                          onMale={this.onMale}
                          onFemale={this.onFemale}
                          managers={this.props.managers}
                          cities={this.props.cities}
                          states={this.props.states}
                          state={this.state}
                          breeds={this.props.breeds.filter((b) => b.name.toLocaleLowerCase().includes(this.state.breedFilter.toLocaleLowerCase()))}
                          tasks={this.props.tasks}
                          onReset={this.onReset}
                          onShowFilters={this.onShowFilters}
                      />)
                      :
                      ''
                  }
				  <div className='admin-footer'>
					  {this.state.showRegForm ?
						  <RegForm onShow={this.onShowRegForm} registrationUser={this.props.registrationUser}/>
						  :
						  null
					  }
					  {this.state.showLogs ?
						  <Logs onShow={this.onLogsShow} logs={this.props.logs}/>
						  :
						  null
					  }
					  {this.props.total_pages > 1 ?
						  <Pagination
							  current={Number(this.state.currentPage)}
							  pageSize={15}
							  total={15*this.props.total_pages}
							  style={{margin: '0 auto', maxWidth: '390px', position: 'fixed', left: '50%', bottom: '2%', transform: 'translate(-50%)' }}
							  onChange={this.onHandlePage}
						  />
						  :
						  null }
					  {this.props.users ? <AdminControlls onAdminControllsDisplay={this.onAdminControllsDisplay} onRankChange={this.props.onRankChange} onGetAllUsersList={this.props.onGetAllUsersList} displayForAdmin={this.state.displayForAdmin} /> : ''}
					  <button className="filters btn btn-primary" onClick={this.onShowFilters}>Фильтры</button>
					  <button className="reset btn btn-primary"  onClick={(ev => this.onReset(ev))}>Сбросить</button>
					  <button className="exit btn btn-primary" onClick={() => {window.localStorage.removeItem("token"); window.location.reload()}}>Выход</button>
					  <button className="reg-user btn btn-primary" onClick={this.onShowRegForm}>Регистрация</button>
					  { this.props.users && this.props.rank === 0 ? <button className="btn btn-primary logs-btn" onClick={() => this.onLogsShow()}>Логи</button> : null }
					  {this.props.users && this.props.rank === 0 ? <button className="admin-btn btn btn-primary" onClick={this.onAdminControllsDisplay}>Доступ</button> : ''}
				  </div>
              </div>
          );
      }
}
const mapState = (state) => {
  return {
    users: state.users ? state.users.results : null,
    states: state.states || [],
    managers: state.managers || [],
    cities: state.cities || [],
    breeds: state.breeds || [],
    tasks: state.tasks.tasks || [],
    usersId: state.selectedUsers || [],
    logs: state.logs || [],
    total_pages: state.users ? state.users.total_pages : 0,
    rank: state.rank
  }
}
const mapDispatch = (dispatch) => {
  return {
    getUsersInf: (page) => dispatch(getUsers(page)),
    getFiltered: (page, fn, cm, em, ct, cn, mn, br, st, tk, mb, pb, bc, gc, wa, vb, tg, fbc, scc, dt, ck) => dispatch(getFilteredUsersInfo(page, fn, cm, em, ct, cn, mn, br, st, tk, mb, pb, bc, gc, wa, vb, tg, fbc, scc, dt, ck)),
    getBreedsList: () => dispatch(getBreeds()),
    getCitiesList: (id) => dispatch(getCities(id)),
    onAddTask: (id, cond, deadline) => dispatch(addTask(id, cond, deadline)),
    onChangeState: (idU, idT) => dispatch(onStateChange(idU, idT)),
    onDeleteTask: (id, cond) => dispatch(deleteTask(id, cond)),
    onChangeManager: (id, mID) => dispatch(changeManager(id, mID)),
    onSelectUser: (id) => dispatch(selectUser(id)),
    onDeselectUser: (id) => dispatch(deselectUser(id)),
    cleanUsersListId: () => dispatch(cleanSelectedUsers()),
    onDeletePair: (id) => dispatch(deletePair(id)),
    onEditPair: (id, cost, date, currency, dad_feat, dad_ped, mom_feat, mom_ped) => dispatch(editPair(id, cost, date, currency, dad_feat, dad_ped, mom_feat, mom_ped)),
    onGetAllUsersList: () => dispatch(getAllUsersList()),
    onRankChange: (id, rank) => dispatch(rankChange(id, rank)),
    onDeleteState: (uId, tId) => dispatch(deleteState(uId, tId)),
    onDeleteKin: (id) => dispatch(deleteKin(id)),
    onEditProfile: (id, first_name, last_name, company, description, facebook, country, city, viber, whats, telegram, inst, mobile, instagram) => dispatch(editUserProfile(id, first_name, last_name, company, description, facebook, country, city, viber, whats, telegram, inst, mobile, instagram)),
    onAddPlanedPair: (breedId, birth, userId) => dispatch(addPlannedPair(breedId, birth, userId)),
    onDeletePlannedPair: (breedId) => dispatch(deletePlannedPair(breedId)),
    onAddFavoriteBreed: (owner, breed) => dispatch(addFavoriteBreed(owner, breed)),
    onAddFavoriteBreedPair: (owner, birthday, currency, breed, cost, boys, girls, breed_id) => dispatch(addFavoriteBreedPair(owner, birthday, currency, breed, cost, boys, girls, breed_id)),
    onChangeComment: (owner, comment) => dispatch(changeUserComment(owner, comment)),
    onAddChild: (pair_id, type) => dispatch(addChild(pair_id, type)),
    lastCall: (owner_id, date) => dispatch(onLastCallChange(owner_id, date)),
    registrationUser: (first_name, email, mobile, animal_id, onReset) => dispatch(regUser(first_name, email, mobile, animal_id, onReset)),
    log: () => dispatch(getLogs()),
    getRank: () => dispatch(getUserRank())
  }
}
export default connect(mapState, mapDispatch)(AdminTable);


