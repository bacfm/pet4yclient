import React from 'react';


export default class RegForm extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            email: '',
            name: '',
            phone: '',
            animal_id: null,
            errorPhone: false
        }
        this.onMail = this.onMail.bind(this);
        this.onName = this.onName.bind(this);
        this.onPhone = this.onPhone.bind(this);
        this.onClick = this.onClick.bind(this);
        this.onReset = this.onReset.bind(this);
    }
    onMail(ev){
        ev.preventDefault();
        this.setState({email: ev.target.value})
    }
    onName(ev){
        ev.preventDefault();
        this.setState({name: ev.target.value})
    }
    onPhone(ev){
        ev.preventDefault();
        this.setState({phone: ev.target.value})
    }
    onReset(){
        this.setState({name: '', phone: '', email: '', animal_id: null})
    }
    onClick(ev){
        ev.preventDefault();
        if (this.state.phone.search(/\+?(38)?0\d{9}/g) === -1 && this.state.phone.search(/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/g) === -1 && this.state.phone.search(/\+?(375)?0\d{9}/g)){
            this.setState({errorPhone: true});
            setTimeout(() => this.setState({errorPhone: false}), 3000);
        } else {
            this.props.registrationUser(this.state.name, this.state.email, this.state.phone, this.state.animal_id, this.onReset);
        }
    }
    render(){
        return (
            <div className="modal" role="dialog" aria-labelledby="exampleModalCenterTitle"
                 aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h4 className="modal-title">Регистрация</h4>
                            <button type="button" className="close" onClick={this.props.onShow} data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <div className="form-group row">
                                <label className="col-sm-3 col-form-label">Имя</label>
                                <div className="col-sm-9">
                                    <input  className="form-control" type="text" placeholder="Имя" value={this.state.name} onChange={this.onName}/>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-sm-3 col-form-label">Телефон</label>
                                <div className="col-sm-9">
                                    <input  className="form-control" type="text" placeholder="Телефон" value={this.state.phone} onChange={this.onPhone}/>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-sm-3 col-form-label">E-mail</label>
                                <div className="col-sm-9">
                                    <input  className="form-control" type="text" placeholder="E-mail" value={this.state.email} onChange={this.onMail}/>
                                </div>
                            </div>
                            <div className="form-group row">
                                <div className="col-sm-12">
                                   <h5 style={{textAlign: 'center', color: (this.state.errorPhone ? "red" : "inherit")}}>Номер телефона должен быть записан через код страны: +7, +380, +375</h5>
                                </div>
                            </div>
                            <div className="form-group row">
                                <div className="col-sm-12">
                                    <div className="form-control" style={{textAlign: 'center', border: 'none'}}>
                                        <button className="btn btn-primary"  style={{marginRight: '15px', padding: '10px 15px'}}  disabled={this.state.animal_id === 1 ? true : false} onClick={() => this.setState({animal_id: 1})}>Собаки</button>
                                        <button className="btn btn-primary" style={{ padding: '10px 15px'}}  disabled={this.state.animal_id === 2 ? true : false} onClick={() => this.setState({animal_id: 2})}>Кошки</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="modal-footer">
                            <button className="btn btn-primary" onClick={this.props.onShow}>Отмена</button>
                            <button className="btn btn-primary" onClick={this.onClick}>Готово</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
