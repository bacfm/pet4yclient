import React from 'react';

export default function AdminItem({u, onRankChange}){
    return (
        <div className="admin-item">
            <span>{u.first_name}</span>
            <span>{u.email}</span>
            <button className="btn btn-primary" onClick={() => onRankChange(u.id, 1)} disabled={u.rank === 1 || u.rank === 0 ? true : false}>Админ</button>
            <button className="btn btn-primary" onClick={() => onRankChange(u.id, 2)} disabled={u.rank === 2 || u.rank === 0 ? true : false}>Менеджер</button>
            <button className="btn btn-primary" onClick={() => onRankChange(u.id, 4)}  disabled={u.rank === null || u.rank === 0 ? true : false}>Пользователь</button>
        </div>
    )
}