import React from 'react';


export default class Logs extends React.Component {
    render(){
        return (
            <div className="modal" role="dialog" aria-labelledby="exampleModalCenterTitle"
                 aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h4 className="modal-title">Регистрация</h4>
                            <button type="button" className="close" onClick={() => this.props.onShow()} data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body" style={{overflow: 'scroll', height: '500px'}}>
                            {this.props.logs.reverse().map((log, idx) => {
                                return ( <div key={idx} className="form-group row">
                                    <label className="col-sm-3 col-form-label">{log.manager_name}</label>
                                    <div className="col-sm-9">
                                        <p  className="form-control">{log.action}</p>
                                        <p  className="form-control">Назначено: {log.created_at}</p>
                                        <p  className="form-control">Пользователь: {log.user_email}</p>
                                    </div>
                                </div>);
                            })}
                        </div>
                        <div className="modal-footer">
                            <button className="btn btn-primary" onClick={() => this.props.onShow()} >Отмена</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
