import React, {Component} from 'react';
import {connect} from 'react-redux';
import AdminItem from './admin-item';

class AdminControlls extends Component {
    constructor(props) {
        super(props);
        this.state = {
            filter: ''
        }
        this.onChange = this.onChange.bind(this);
    }

    componentDidMount() {
        this.props.onGetAllUsersList();
    }

    onChange(ev) {
        this.setState({filter: ev.target.value})
    }

    render() {
        if (this.props.users) {
            return (
                <div className={"admin-block container" + (this.props.displayForAdmin ? '' : ' hidden-a')}>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={(ev) => this.props.onAdminControllsDisplay(ev)}>
                        <span>&times;</span>
                    </button>
                    <input type="text" className="form-control" placeholder="Фильтр" value={this.state.filter} onChange={this.onChange}/>
                    <div className="admin-block-wrap">
                        {this.props.users.filter((u) => u.email.includes(this.state.filter) || u.first_name.includes(this.state.filter)).map((i, idx) =>
                            <AdminItem onRankChange={this.props.onRankChange} u={i} key={idx}/>)}
                    </div>
                </div>
            );
        } else {
            return (<div></div>)
        }
    }
}

const mapState = (state) => {
    return {
        users: state.allUsersList
    }
}

export default connect(mapState)(AdminControlls)
