import React, { Component } from 'react';
import Calendar from '../Calendar';
import './style.css';

class Filters extends Component {
    render(){
        const { state } = this.props;
        return (
            <div className={"filters-block container  flex-column" + (this.props.display ? '' : ' hidden-f')}>
                <button type="button" className="close" style={{top: '-15px', right: '5px'}} data-dismiss="modal" aria-label="Close" onClick={(ev) => this.props.onShowFilters(ev)}>
                    <span aria-hidden="true">&times;</span>
                </button>
                    <select className="form-control" value={state.country} onChange={(ev) => this.props.onCountry(ev)}>
                        <option value="">Выберите страну</option>
                        <option value="1">Украина</option>
                        <option value="2">Россия</option>
						<option value="3">Беларусь</option>
                    </select>
                    <select className="form-control" value={state.city} onChange={(ev) => this.props.onCity(ev)}>
                        <option value="">Выберите город</option>
                        {this.props.cities.map((c, idx) => <option key={idx} value={c.name}>{c.name}</option>)}
                    </select>
                    <select className="form-control" value={state.manager} onChange={(ev) => this.props.onManager(ev)}>
                        <option value="">Выберите менеджера</option>
                        {this.props.managers.map((m, idx) => <option key={idx} value={m.id}>{m.name}</option>)}
                    </select>
                    <select className="form-control" value={state.task} onChange={(ev) => this.props.onTask(ev)}>
                        <option value="">Выберите задание</option>
                        {this.props.tasks.map((t, idx) => <option key={idx} value={t.id}>{t.title}</option>)}
                    </select>
                    <Calendar date={state.filterDate} handleFromChange={this.props.onFilterDateChange}/>
                    <select className="form-control" value={state.state} onChange={(ev) => this.props.onState(ev)}>
                        <option value="">Выберите статус</option>
                        {this.props.states.map((s, idx) => <option key={idx} value={s.id}>{s.title}</option>)}
                    </select>
                    <input type="text" className="form-control" placeholder="Идентификатор щенка" value={state.dog_name_key} onChange={(ev) => this.props.onDogKey(ev)}/>
                    <div className="breed-input">
                        <input className="form-control" value={state.breedFilter} type="text" placeholder="Порода" onChange={(ev) => this.props.onBreedFilter(ev)}/>
                        <ul className="breeds-list list-group">
                            {this.props.breeds.map((b, idx) => <li className="list-group-item" key={idx} onClick={() => {this.props.onBreed(b.id); this.props.onSetBreed(b.name)}}>{b.name}</li>)}
                        </ul>
                    </div>
                    <input className="form-control" value={state.personName} type="text" placeholder="ФИО" onChange={(ev) => this.props.onName(ev)}/>
                    <input className="form-control" value={state.email} type="text" placeholder="E-mail" onChange={(ev) => this.props.onEmail(ev)}/>
                    <input className="form-control" value={state.company} type="text" placeholder="Питомник" onChange={(ev) => this.props.onCompany(ev)}/>
                    <input className="form-control" value={state.phone} type="text" placeholder="Телефон" onChange={(ev) => this.props.onPhone(ev)}/>
                <div style={{overflow: (window.innerHeight <= 665 ? 'scroll': 'inherit'), height: (window.innerHeight <= 665 ? '70px': 'inherit')}}>
                    <div className="form-check">
                        <input className="form-check-input" checked={state.planned} type="checkbox" onChange={(ev) => this.props.onPlanned(ev)} />
                            <label className="form-check-label">Запланированный помет</label>
                    </div>
                    <div className="form-check">
                        <input className="form-check-input"  checked={state.male} type="checkbox" onChange={(ev) => this.props.onMale(ev)} />
                        <label className="form-check-label">Мальчики</label>
                    </div>
                    <div className="form-check">
                        <input className="form-check-input"  checked={state.female} type="checkbox" onChange={(ev) => this.props.onFemale(ev)} />
                        <label className="form-check-label" >Девочки</label>
                    </div>
                <div className="form-check">
                    <input className="form-check-input"  checked={state.viber} type="checkbox" onChange={(ev) => this.props.onViber(ev)} />
                    <label className="form-check-label" >Viber</label>
                </div>
                <div className="form-check">
                    <input className="form-check-input"  checked={state.whatsapp} type="checkbox" onChange={(ev) => this.props.onWhats(ev)} />
                    <label className="form-check-label" >WhatsApp</label>
                </div>
                <div className="form-check">
                    <input className="form-check-input"  checked={state.telegram} type="checkbox" onChange={(ev) => this.props.onTelegram(ev)} />
                    <label className="form-check-label" >Telegram</label>
                </div>
                <div className="form-check">
                    <input className="form-check-input"  checked={state.facebook} type="checkbox" onChange={(ev) => this.props.onFacebook(ev)} />
                    <label className="form-check-label" >Facebook</label>
                </div>
                <div className="form-check">
                    <input className="form-check-input"  checked={state.anotherSocial} type="checkbox" onChange={(ev) => this.props.onAnotherSoc(ev)} />
                    <label className="form-check-label" >Др. соц. сети</label>
                </div>
                </div>
                <div className="filter-controlls">
                    <button className="btn btn-primary"  onClick={(ev => {this.props.onFilter(ev, state.profileChoose); this.props.onShowFilters(ev)})}>Найти</button>
                </div>

            </div>
        );
    }
}

export default Filters;
