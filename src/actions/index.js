export const fetchUsers = (res) => ({
    type: 'GET_USERS',
    payload: res
});
export const fetchFilteredUsersInfo = (res) => ({
    type: 'FILTER_INFO',
    payload: res
})

export const  fetchManagers = (res) => ({
    type: 'GET_MANAGERS',
    payload: res
})
export const fetchStates = (res) => ({
    type: 'GET_STATES',
    payload: res
})
export const fetchCities = (res) => ({
    type: 'GET_CITIES',
    payload: res
})
export const fetchBreeds = (res) => ({
    type: 'GET_BREEDS',
    payload: res
})
export const fetchTasks = (res) => ({
    type: 'GET_TASKS',
    payload: res
})

export const selectUser = (id) => ({
    type: 'SELECT_USER',
    id: id
})

export const deselectUser = (id) => ({
    type: 'DESELECT_USER',
    id: id
})
export const cleanSelectedUsers = () => ({
    type: 'CLEAN_SELECTED_USERS'
})
export const fetchAllUsers = (res) => ({
    type: 'GET_ALL_USERS_LIST',
    payload: res
})

export const fetchLogs = (res) => ({
    type: 'GET_LOGS',
    payload: res
})

export const fetchRank = (res) => ({
    type: 'GET_USERS_RANK',
    payload: res.rank
})
