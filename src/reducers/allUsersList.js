const initialState = null;

export const allUsersList = (state=initialState, action) => {
    if(action.type === 'GET_ALL_USERS_LIST'){ return action.payload }
    return state;
}