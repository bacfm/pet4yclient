const initialState = [];

export const selectedUsers = (state=initialState, action) => {
    if(action.type === 'SELECT_USER'){ return selectUser(state, action)}
    else if(action.type === 'DESELECT_USER'){ return deselectUser(state, action)}
    else if(action.type === 'CLEAN_SELECTED_USERS'){ return []}
    else {
        return state;
    }
}

function selectUser(state, action) {
    const { id } = action;
    return [...state, id]
}
function deselectUser(state, action){
    const { id } = action;
    const userIndex = state.findIndex(index => index === id);
    return [
        ...state.slice(0, userIndex),
        ...state.slice(userIndex + 1)
    ]
}