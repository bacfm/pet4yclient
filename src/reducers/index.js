import { combineReducers } from 'redux';
import { users } from './userInfo';
import { managers } from './managers';
import { states } from './states';
import { cities } from './cities';
import { breeds } from './breeds';
import { tasks } from './tasks';
import { selectedUsers } from './selectedUsers';
import { allUsersList } from './allUsersList'
import { logs } from './logs';
import { rank } from './rank';

export default combineReducers({
    users,
    managers,
    states,
    cities,
    breeds,
    tasks,
    selectedUsers,
    allUsersList,
    logs,
	rank
});
