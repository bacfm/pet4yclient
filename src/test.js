<div className="admin-table">
    <table className="table table-bordered table-sm ">
        <thead>
        <tr scope='row'>
            <th scope="col"></th>
            <th scope="col">Статус</th>
            <th scope="col">Менеджер</th>
            <th scope="col">Задание</th>
            <th>Питомник</th>
            <th scope="col">Страна</th>
            <th scope="col">Город</th>
            <th scope="col">ФИО</th>
            <th scope="col">Телефон</th>
            <th scope="col">E-mail</th>
            <th scope="col">Соц. сети</th>
            <th scope="col">Породы</th>
            <th scope="col">Пометы</th>
        </tr>
        </thead>
        <tbody>
        <tr scope='row'>
            <th scope="col">
                <input type="checkbox"/>
            </th>
            <th scope="col">
                <select>
                    <option>Новый клиент</option>
                    <option>Постоянный клиент</option>
                </select>
            </th>
            <th scope="col">
                <select>
                    <option>Менеджер 1</option>
                    <option>Менеджер 2</option>
                </select>
            </th>
            <th scope="col">
                <select>
                    <option>Перезвонить</option>
                    <option>Написать</option>
                </select>
            </th>
            <th>Название питомника</th>
            <th scope="col">
                Украина
            </th>
            <th scope="col">
                Днепр
            </th>
            <th scope="col">
                Пупкин Василий Васькович
            </th>
            <th scope="col">
                +380999997788
            </th>
            <th scope="col">
                test@gmail.com
            </th>
            <th scope="col">
                <table className="table table-bordered social">
                    <tbody>
                    <tr>
                        <th>icon</th>
                        <th>link</th>
                    </tr>
                    <tr>
                        <th>icon</th>
                        <th>link</th>
                    </tr>
                    <tr>
                        <th>icon</th>
                        <th>link</th>
                    </tr>
                    </tbody>
                </table>
            </th>
            <th scope="col">
                <select>
                    <option value="">Овчарка</option>
                    <option value="">Бигль</option>
                </select>
            </th>
            <th scope="col">
                Пометы
                <table>
                    <thead>
                    <tr>
                        <th colSpan={7}>Пометы</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Бигль</td>
                        <td>18.09.2018</td>
                        <td>М5</td>
                        <td>F3</td>
                        <td>К</td>
                        <td>У</td>
                        <td>С</td>
                    </tr>
                    </tbody>
                </table>
            </th>
        </tr>
        </tbody>
    </table>
    <button className="show-Filters" onClick={this.onDisplayFilter}>Фильтры</button>
    <Filters display={this.state.filters}/>
</div>