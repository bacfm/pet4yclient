import {
    fetchUsers,
    fetchFilteredUsersInfo,
    fetchManagers,
    fetchStates,
    fetchCities,
    fetchBreeds,
    fetchTasks,
    fetchAllUsers,
    fetchLogs,
	fetchRank
} from '../actions';

const baseUrl = 'http://api.pet4u.com.ua/api/v1/panel/';

export function getUsers(page) {
    let ai;
    if (window.localStorage.pet_prof) {
        ai = window.localStorage.pet_prof
    } else {
        ai = 1;
    }
    if (!page && window.localStorage.page) {
        page = window.localStorage.page;
    } else if (page) {
        page = page;
    } else {
        page = 1;
    }
    return (dispatch) => {
        return fetch(baseUrl + `new_users?page=${page}&ai=${ai}`, {
            method: 'GET',
            headers: {
                'Authorization': "Token " + window.localStorage.token,
                'Content-type': 'application/json'
            }
        })
            .then(res => res.json())
            .then(res => dispatch(fetchUsers(res)))
            .catch(err => console.log(err))
    }
}

export function getUserRank () {
    return (dispatch) => {
		return fetch(baseUrl + 'user-rank', {
			method: 'GET',
			headers: {
				'Authorization': "Token " + window.localStorage.token,
				'Content-type': 'application/json'
			}
		})
			.then(res => res.json())
			.then(res => dispatch(fetchRank(res)))
			.catch(err => console.log(err))
	}
}

export function getFilteredUsersInfo(page, fn, cm, em, ct, cn, mn, br, st, tk, mb, pb, bc, gc, wa, vb, tg, fbc, scc, dt, ck) {
    let ai;
    if (window.localStorage.pet_prof) {
        ai = window.localStorage.pet_prof
    } else {
        ai = 1;
    }
	if (!page && window.localStorage.page) {
		page = window.localStorage.page;
	} else if (page) {
		page = page;
	} else {
	    page = 1;
    }
    return (dispatch) => {
        return fetch(baseUrl + `new_users?page=${page}&ai=${ai}&fn=${fn}&cm=${cm}&em=${em}&ct=${ct}&cn=${cn}&mn=${mn}&br=${br}&st=${st}&tk=${tk}&mb=${mb}&pb=${pb}&bc=${bc}&gc=${gc}&wa=${wa}&vb=${vb}&tg=${tg}&fbc=${fbc}&scc=${scc}&dt=${dt}&ck=${ck}`, {
            method: 'GET',
            headers: {
                'Authorization': "Token " + window.localStorage.token,
                'Content-type': 'application/json'
            }
        })
            .then(res => res.json())
            .then(res => dispatch(fetchFilteredUsersInfo(res)))
            .catch(err => console.log(err))
    }
}

export function getManagersList() {
    return (dispatch) => {
        return fetch(baseUrl + 'managers', {
            method: 'GET',
            headers: {
                'Authorization': "Token " + window.localStorage.token,
                'Content-type': 'application/json'
            }
        })
            .then(res => {
                if (res.status === 401) {
                    window.localStorage.removeItem('token');
                }
                return res;
            })
            .then((res) => res.json())
            .then(res => dispatch(fetchManagers(res)))
            .catch(err => console.log(err))
    }
}

export function getStates() {
    return (dispatch) => {
        return fetch(baseUrl + 'states', {
            method: 'GET',
            headers: {
                'Authorization': "Token " + window.localStorage.token,
                'Content-type': 'application/json'
            }
        })
            .then(res => res.json())
            .then(res => dispatch(fetchStates(res)))
            .catch(err => console.log(err))
    }
}

export function getCities(id) {
    if (!id) {
        id = 1;
    }
    return (dispatch) => {
        return fetch(baseUrl + `cities?id=${id}`, {
            method: 'GET',
            headers: {
                'Authorization': "Token " + window.localStorage.token,
                'Content-type': 'application/json'
            }
        })
            .then(res => res.json())
            .then(res => dispatch(fetchCities(res)))
            .catch(err => console.log(err))
    }
}

export function getBreeds() {
    let id;
    if (window.localStorage.pet_prof) {
        id = window.localStorage.pet_prof
    } else {
        id = 1;
    }
    return (dispatch) => {
        return fetch(baseUrl + `breeds?id=${id}`, {
            method: 'GET',
            headers: {
                'Authorization': "Token " + window.localStorage.token,
                'Content-type': 'application/json'
            }
        })
            .then(res => {
                if (res.status === 401) {
                    window.localStorage.removeItem('token');
                }
                return res;
            })
            .then(res => res.json())
            .then(res => dispatch(fetchBreeds(res)))
            .catch(err => console.log(err))
    }
}

export function getTasks() {
    return (dispatch) => {
        return fetch(baseUrl + 'tasks', {
            method: 'GET',
            headers: {
                'Authorization': "Token " + window.localStorage.token,
                'Content-type': 'application/json'
            }
        })
            .then(res => {
                if (res.status === 401) {
                    window.localStorage.removeItem('token');
                }
                return res;
            })
            .then((res) => res.json())
            .then(res => dispatch(fetchTasks(res)))
            .catch(err => console.log(err))
    }
}

export function addTask(id, condition, deadline) {
    let link = deadline ? baseUrl + `tasks?id=${id}&mission=${condition}&deadline=${deadline}` : baseUrl + `tasks?id=${id}&mission=${condition}`;
    return (dispatch) => {
        return fetch(link, {
            method: 'PUT',
            headers: {
                'Authorization': "Token " + window.localStorage.token,
                'Content-type': 'application/json'
            }
        })
            .then(res => {
                if (res.status === 200) {
                    dispatch(getLogs());
                    dispatch(getUsers());
                } else {
                    console.log('ERROR')
                }
            })
            .catch((err) => console.log(err))
    }
}

export function deleteTask(id, condition) {
    return (dispatch) => {
        return fetch(baseUrl + `tasks?id=${id}&mission=${condition}`, {
            method: 'DELETE',
            headers: {
                'Authorization': "Token " + window.localStorage.token,
                'Content-type': 'application/json'
            }
        })
            .then(res => {
                if (res.status === 200) {
                    dispatch(getLogs());
                    dispatch(getUsers());
                } else {
                    console.log('ERROR')
                }
            })
            .catch((err) => console.log(err))
    }
}

export function onStateChange(idU, idT) {
    return (dispatch) => {
        return fetch(baseUrl + `states?id=${idU}&condition=${idT}`, {
            method: 'PUT',
            headers: {
                'Authorization': "Token " + window.localStorage.token,
                'Content-type': 'application/json'
            }
        })
            .then(res => {
                if (res.status === 200) {
                    dispatch(getLogs());
                    dispatch(getUsers());
                } else {
                    console.log('ERROR')
                }
            })
            .catch((err) => console.log(err))
    }
}

export function deleteState(idU, idT) {
    return (dispatch) => {
        return fetch(baseUrl + `states?id=${idU}&condition=${idT}`, {
            method: 'DELETE',
            headers: {
                'Authorization': "Token " + window.localStorage.token,
                'Content-type': 'application/json'
            }
        })
            .then(res => {
                if (res.status === 200) {
                    dispatch(getLogs());
                    dispatch(getUsers());
                } else {
                    console.log('ERROR')
                }
            })
            .catch((err) => console.log(err))
    }
}

export function changeManager(idU, managerID) {
    return (dispatch) => {
        return fetch(baseUrl + `managers?id=${idU}&manager=${managerID}`, {
            method: 'PUT',
            headers: {
                'Authorization': "Token " + window.localStorage.token,
                'Content-type': 'application/json'
            }
        })
            .then(res => {
                if (res.status === 200) {
                    dispatch(getLogs());
                    dispatch(getUsers());
                } else {
                    console.log('ERROR')
                }
            })
            .catch((err) => console.log(err))
    }
}

export function deletePair(id) {
    return (dispatch) => {
        return fetch(baseUrl + `pair?id=${id}`, {
            method: 'DELETE',
            headers: {
                'Authorization': "Token " + window.localStorage.token,
                'Content-type': 'application/json'
            }
        })
            .then(res => {
                if (res.status === 200) {
                    dispatch(getLogs());
                    dispatch(getUsers());
                } else {
                    console.log('ERROR')
                }
            })
    }
}

export function editPair(id, cost, date, currency, dad_feat, dad_ped, mom_feat, mom_ped) {
    const body = {
        cost: cost,
        birthday: date,
        currency: currency,
        parents: {father: {feat: dad_feat, pedigree: dad_ped}, mother: {feat: mom_feat, pedigree: mom_ped}}
    }
    return (dispatch) => {
        return fetch(baseUrl + `pair?id=${id}`, {
            method: 'PUT',
            headers: {
                'Authorization': "Token " + window.localStorage.token,
                'Content-type': 'application/json'
            },
            body: JSON.stringify(body)
        })
            .then(res => {
                if (res.status === 200) {
                    dispatch(getLogs());
                    dispatch(getUsers());
                } else {
                    console.log('ERROR')
                }
            })
    }
}

export function getAllUsersList() {
    return (dispatch) => {
        return fetch(baseUrl + 'userlist', {
            method: 'GET',
            headers: {
                'Authorization': "Token " + window.localStorage.token,
                'Content-type': 'application/json'
            }
        })
            .then(res => res.json())
            .then(res => dispatch(fetchAllUsers(res)))
            .catch(err => console.log(err))
    }
}

export function rankChange(id, rank) {
    return (dispatch) => {
        return fetch(baseUrl + `managers?id=${id}&rank=${rank}`, {
            method: 'POST',
            headers: {
                'Authorization': "Token " + window.localStorage.token,
                'Content-type': 'application/json'
            }
        })
            .then(res => {
                if (res.status === 200) {
                    dispatch(getLogs());
                    dispatch(getManagersList());
                    dispatch(getAllUsersList());
                }
            })
            .catch(err => console.log(err))
    }
}

export function deleteKin(id) {
    return dispatch => {
        return fetch(baseUrl + `child?id=${id}`, {
            method: 'DELETE',
            headers: {
                'Authorization': "Token " + window.localStorage.token,
            }
        })
            .then(res => {
                if (res.status === 200) {
                    dispatch(getLogs());
                    return dispatch(getUsers());
                } else {
                    console.log('ERROR')
                }
            })
            .catch(err => console.log(err))
    }
}

export function addUserPair() {
    return dispatch => {
        return fetch('http://api.pet4u.com.ua/api/v1/panel/pair', {
            method: 'PUT',
            headers: {
                'Authorization': "Token " + window.localStorage.token,
                'Content-type': 'application/json'
            }
        })
    }
}

export function editUserProfile(id, first_name, last_name, company, description, facebook, country, city, viber, whats, telegram, inst, mobile, instagram) {
    const body = {
        first_name,
        last_name,
        company,
        description,
        social: instagram,
        facebook,
        country,
        city,
        mobile,
        feedback: {viber, whatsapp: whats, telegram, social: inst}
    }
    return dispatch => {
        return fetch(baseUrl + `profile?id=${id}`, {
            method: 'PUT',
            headers: {
                'Authorization': "Token " + window.localStorage.token,
                'Content-type': 'application/json'
            },
            body: JSON.stringify(body)
        })
            .then(res => {
                if (res.status === 200) {
                    dispatch(getLogs());
                    dispatch(getUsers())
                } else {
                    console.log('ERROR')
                }
            })
            .catch((err) => console.log(err))
    }
}

export function addPlannedPair(breedId, birthday, userId) {
    const body = {breed: breedId, birthday: birthday.getFullYear() + '-' + (Number(birthday.getMonth()) + 1) + '-' + birthday.getDate(), owner_id: userId};
    return dispatch => {
        return fetch(baseUrl + 'planned', {
            method: 'POST',
            headers: {
                'Authorization': "Token " + window.localStorage.token,
                'Content-type': 'application/json'
            },
            body: JSON.stringify(body)
        })
            .then(res => {
                if (res.status === 201) {
                    dispatch(getLogs());
                    return dispatch(getUsers());
                } else {
                    console.log('ERROR')
                }
            })
            .catch(err => console.log(err))
    }
}

export function deletePlannedPair(breedId) {
    return dispatch => {
        return fetch(baseUrl + `planned?id=${breedId}`, {
            method: 'DELETE',
            headers: {
                'Authorization': "Token " + window.localStorage.token,
                'Content-type': 'application/json'
            },
        })
            .then(res => {
                if (res.status === 200) {
                    dispatch(getLogs());
                    return dispatch(getUsers());
                } else {
                    console.log('ERROR')
                }
            })
            .catch(err => console.log(err))
    }
}

export function addFavoriteBreed(owner, breed) {
    const body = {owner_id: owner, breed: breed};
    return dispatch => {
        return fetch(baseUrl + 'favorite', {
            method: 'POST',
            headers: {
                'Authorization': "Token " + window.localStorage.token,
                'Content-type': 'application/json'
            },
            body: JSON.stringify(body)
        })
            .then(res => {
                if (res.status === 201) {
                    dispatch(getLogs());
                    dispatch(getUsers())
                } else {
                    console.log('ERROR');
                }
            })
            .catch(err => console.log(err))

    }
}

export function addFavoriteBreedPair(owner, birthday, currency, breed, cost, boys, girls, breed_id) {
    const body = {owner_id: owner, birthday, currency, breed, cost, boys: Number(boys), girls: Number(girls), breed_id};
    return dispatch => {
        return fetch(baseUrl + 'pair', {
            method: 'POST',
            headers: {
                'Authorization': "Token " + window.localStorage.token,
                'Content-type': 'application/json'
            },
            body: JSON.stringify(body)
        })
            .then(res => {
                if (res.status === 201) {
                    dispatch(getLogs());
                    return dispatch(getUsers())
                } else {
                    console.log('ERROR')
                }
            })
            .catch(err => console.log(err))
    }
}

export function changeUserComment(owner_id, comment) {
    const body = {owner_id, comment};
    return dispatch => {
        return fetch(baseUrl + 'comment', {
            method: 'PUT',
            headers: {
                'Authorization': "Token " + window.localStorage.token,
                'Content-type': 'application/json'
            },
            body: JSON.stringify(body)
        })
            .then(res => {
                if (res.status === 200) {
                    dispatch(getLogs());
                    return dispatch(getUsers())
                } else {
                    console.log('ERROR')
                }
            })
            .catch(err => console.log(err))
    }
}

export function addChild(pair_id, type){
    const body = {pair_id, type};
    return dispatch => {
        return fetch(baseUrl + 'child',{
            method: 'POST',
            headers: {
                'Authorization': "Token " + window.localStorage.token,
                'Content-type': 'application/json'
            },
            body: JSON.stringify(body)
        })
            .then (res => {
                if(res.status === 200){
                    dispatch(getLogs());
                    return dispatch(getUsers())
                } else {
                    console.log('ERROR')
                }
            })
            .catch(err => console.log(err))
    }
}

export function onLastCallChange(owner_id, date){
    const body = {owner_id, date};
    return dispatch => {
        return fetch(baseUrl + 'called', {
            method: 'POST',
            headers: {
                'Authorization': "Token " + window.localStorage.token,
                'Content-type': 'application/json'
            },
            body: JSON.stringify(body)
        })
            .then(res => {
                if (res.status === 200) {
                    dispatch(getLogs());
                    return dispatch(getUsers())
                } else {
                    console.log('ERROR')
                }
            })
            .catch(err => console.log(err))
    }
}

export function regUser(first_name, email, mobile, animal_id, onReset){
    const body = {first_name, email, animal_id, mobile};
    return dispatch => {
        return fetch(baseUrl + 'reg', {
            method: 'POST',
            headers: {
                'Authorization': "Token " + window.localStorage.token,
                'Content-type': 'application/json'
            },
            body: JSON.stringify(body)
        })
            .then(res => {
                if (res.status === 201) {
                    onReset();
                    alert('Аккаунт успешно зарегистрирован!')
                    dispatch(getLogs());
                    return dispatch(getUsers());
                } else {
                    alert('Повторите еще раз');
                    console.log('ERROR')
                }
            })
            .catch(err => console.log(err))
    }
}

export function getLogs(){
    return dispatch => {
        return fetch(baseUrl + 'logs', {
            method: 'GET',
            headers: {
                'Authorization': "Token " + window.localStorage.token,
            },
        })
            .then(res => res.json())
            .then(res => dispatch(fetchLogs(res)))
            .catch(err => console.log(err))
    }
}
