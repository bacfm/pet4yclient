import React, {Component} from 'react';
import AdminTable from './components/Admin-table/index';
import LogForm from './components/loginForm';
import { connect } from 'react-redux';
import {getManagersList,getStates,getCities,getBreeds,getTasks} from './api'
import './App.css';

class App extends Component {
    componentWillMount(){
        this.props.getManagers();
        this.props.getStatus();
        this.props.getCitiesList();
        this.props.getBreedsList();
        this.props.getTasksList();
    }
    render() {
     if(!window.localStorage.token || window.localStorage.token === 'undefined'){
            return (<LogForm />)
        }else{
            return (<AdminTable />)
        }
    }
}

const mapDispatch = (dispatch) => {
    return {
        getManagers: () => dispatch(getManagersList()),
        getStatus: () => dispatch(getStates()),
        getCitiesList: () => dispatch(getCities()),
        getBreedsList: () => dispatch(getBreeds()),
        getTasksList: () => dispatch(getTasks())
    }
}
export default connect(null, mapDispatch)(App);
